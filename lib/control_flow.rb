# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char{ |char| str.delete!(char) if char == char.downcase }
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  i = str.length / 2
  if str.length.odd?
    middle = i
  else
    middle = (i - 1..i)
  end
  str[middle]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char do |char|
    count += 1 if VOWELS.include? char
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = 1
  (1..num).each{ |n| result *= n }
  result
end

# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ''
  arr.each_with_index do |x, i|
    if i == (arr.length - 1)
      result += x
    else
    result += x + separator
    end
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = ''
  str.chars.each_with_index do |char, i|
    i.even? ? result << char.downcase : result << char.upcase
  end
  result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = []
  words = str.split
  words.each do |word|
    if word.length > 4
      result << word.reverse
    else
      result << word
    end
  end
  result.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  (1..n).step do |num|
    if (num % 3).zero? && (num % 5).zero?
      result << 'fizzbuzz'
    elsif (num % 3).zero?
      result << 'fizz'
    elsif (num % 5).zero?
      result << 'buzz'
    else
      result << num
    end
  end
  result
end

# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  i = 0
  while i <= (arr.length + 1)
    take = arr.pop
    result << take
    i += 1
  end
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  count = 0
  (1..num).each do |n|
    next if n == 1
    count += 1 if (num % n).zero?
  end
  return false if count != 1
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  (num + 1).times do |x|
    next if x.zero?
    result << x if (num % x).zero?
  end
  result.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  result = []
  factors_arr = factors(num)
  factors_arr.each do |n|
    result << n if prime? n
  end
  result.sort
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  evens = []
  odds = []
  arr.each { |n| n.even? ? evens << n : odds << n }
  evens.length == 1 ? evens[0] : odds[0]
end
